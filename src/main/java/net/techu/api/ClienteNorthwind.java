package net.techu.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClienteNorthwind implements Serializable {

    @JsonProperty("PersonID")
    private String id;
    @JsonProperty("Age")
    private Integer edad;
    @JsonProperty("Phone")
    private String telefono;
}
