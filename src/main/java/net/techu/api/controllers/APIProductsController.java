package net.techu.api.controllers;

import net.techu.api.GestorREST;
import net.techu.api.ListaProductNorthwind;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RestController
public class APIProductsController {



    @GetMapping(value="/apiproducts")
    public ResponseEntity<ListaProductNorthwind> getProducts() {
        final String uri = "https://services.odata.org/V4/OData/OData.svc/Products?$select=ID,Name,Price";
        GestorREST gestor = new GestorREST();
        RestTemplate plantilla = gestor.crearClienteREST(new RestTemplateBuilder());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Usuario", "AppTechU");
        HttpEntity<String> entity = new HttpEntity<String>(headers);
            ResponseEntity<ListaProductNorthwind> respuesta = plantilla.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<ListaProductNorthwind>() {});
            ListaProductNorthwind lista = plantilla.getForObject(uri, ListaProductNorthwind.class);

            return respuesta;

            //return response.getBody();

    }
}
