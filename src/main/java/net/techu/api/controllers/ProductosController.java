package net.techu.api.controllers;

import net.techu.api.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.json.simple.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductosController {
    private static Map<String, Producto> listaProductos = new HashMap<>();
    //Inicializar lista de Productos
    static{
        Producto pr1 = new Producto();
        pr1.setId("PR1");
        pr1.setNombre("PRODUCTO 01");
        listaProductos.put(pr1.getId(),pr1);

        Producto pr2 = new Producto();
        pr2.setId("PR2");
        pr2.setNombre("PRODUCTO 02");
        listaProductos.put(pr2.getId(),pr2);
    }

    @GetMapping(value = "/productos")
    //@RequestMapping(value = "/productos",method = RequestMethod.GET)
    public Map<String,Producto> getProductos(){
        return listaProductos;
    }

    @Autowired
    ResourceLoader cargador;

    @GetMapping(value = "/v2/productos")
    public String getProductosFile(){
        String fileName="classpath:static/productos.json";
        Resource resourceFile=cargador.getResource(fileName);
        String productos = "";
        try {
            InputStream streamDatos = resourceFile.getInputStream();
            byte[] byteDocumento = streamDatos.readAllBytes();
             productos = new String(byteDocumento, StandardCharsets.UTF_8);
             streamDatos.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return productos;
    }


    @GetMapping(value = "/v3/productos")
    public String getProductosFromAPI(){
        String urlAPI = "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
        String productos = "";
        try {
            URL url = new URL(urlAPI);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("User-Agent", "MiApp");
            conexion.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            InputStreamReader entrada = new InputStreamReader(conexion.getInputStream());
            int respuesta = conexion.getResponseCode();
            System.out.println(new StringBuilder().append("Respuesta : ").append(respuesta));
            if (respuesta == HttpURLConnection.HTTP_OK) {
                BufferedReader lector = new BufferedReader(entrada);
                String lineaLeida;
                StringBuffer resultado = new StringBuffer();
                while ((lineaLeida = lector.readLine()) != null) {
                    resultado.append(lineaLeida);
                }
                lector.close();
                productos = resultado.toString();
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productos;
    }

    @PostMapping(value = "/v3/productos/{formato}")
    public ResponseEntity<Object> addProductoFichero(@RequestBody Producto productoNuevo, @PathVariable("formato") String formato)
    {

        String nombreFichero = "classpath:static/productos.txt";
        Resource recursoFichero = cargador.getResource(nombreFichero);
        if (formato.toUpperCase().equals("JSON")) {
            JSONObject nuevo = new JSONObject();
            nuevo.put("id", productoNuevo.getId());
            nuevo.put("nombre", productoNuevo.getNombre());
            File fichero = null;
            try {
                fichero = recursoFichero.getFile();
                OutputStreamWriter escritor = new FileWriter(fichero, true);
                escritor.append("\n" + nuevo.toString());
                escritor.close();
                return new ResponseEntity<>("Producto JSON añadido al fichero", HttpStatus.CREATED);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else {
            try {
                File fichero = recursoFichero.getFile();
                OutputStreamWriter escritor = new FileWriter(fichero, true);
                escritor.append(String.format("\nNuevo producto: %s %s", productoNuevo.getId(), productoNuevo.getNombre()));
                escritor.flush(); // Vuelco el contenido de memoria a disco
                escritor.close(); // Vuelco el contenido de memoria a disco y cierro el fichero
                return new ResponseEntity<>("Producto añadido al fichero", HttpStatus.CREATED);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>("Error al añadir al fichero", HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/productos/{id}",method = RequestMethod.GET)
    public ResponseEntity<Object> getProducto(@PathVariable("id") String id){
        if(listaProductos.containsKey(id))
            return new ResponseEntity<>(listaProductos.get(id), HttpStatus.OK);
        return new ResponseEntity<>(String.format("No he encontrado el producto %s",id), HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/productos")
    public ResponseEntity<Object> addProducto(@RequestBody Producto nuevoProducto){
        if(listaProductos.containsKey(nuevoProducto.getId()))
            return new ResponseEntity<>(String.format("Producto %s ya existe",nuevoProducto.getId()),HttpStatus.CONFLICT);
        listaProductos.put(nuevoProducto.getId(),nuevoProducto);
        return new ResponseEntity<>(String.format("Productp %s registrado",nuevoProducto.getId()),HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<Object> updateProducto(@PathVariable("id") String id,@RequestBody Producto updProducto){
        if(listaProductos.containsKey(id)) {
            listaProductos.put(id, updProducto);
            return new ResponseEntity<>(String.format("Producto %s actualizado", id), HttpStatus.OK);
        }else
            listaProductos.put(id,updProducto);
            return new ResponseEntity<>(String.format("Producto %s creado",id),HttpStatus.OK);

    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<Object> deleteProducto(@PathVariable("id") String id){
        if(listaProductos.containsKey(id)) {
            listaProductos.remove(id);
            return new ResponseEntity<>(String.format("Producto %s eliminado", id), HttpStatus.OK);
        }else
            return new ResponseEntity<>(String.format("Producto %s no encontrado",id),HttpStatus.NOT_FOUND);
    }

    //@RequestMapping("/productos")
    public String productos(){
        return "Lista de Productos";
    }

    //@RequestMapping("/productos")
    public String getProductosByID(@RequestParam(value = "id", required = true) String id){
        return String.format("Info del Producto %s", id);
    }

}
